SET IDENTITY_INSERT Powers ON
-- Creating the powers
INSERT INTO Powers (ID, FullName, PowerDescription) VALUES
(1, 'Flight', 'The hero is able to take flight and fly whereever they please'),
(2, 'Laser Eyes', 'The hero is able to shoot laser out through their eyes'),
(3, 'Invisibility', 'The hero can turn invisible at any point they please'),
(4, 'Gravitation', 'The hero can harness the power of gravity!');
SET IDENTITY_INSERT Powers OFF

-- Giving powers to the supes
INSERT INTO PowerSuperheroRelation (PowerID, SuperheroID) VALUES
(1, 1), -- Flight to Homelander
(2, 1), -- Laser eyes to Homelander
(2, 2), -- Laser eyes to Stormfront
(3, 3), -- Invisibility to Starlight
(4, 3)  -- Gravitation to Starlight