SET IDENTITY_INSERT Superheroes ON

INSERT INTO Superheroes (ID, FullName, Alias) VALUES
(1, 'John', 'Homelander'),
(2, 'Klara Risinger', 'Stormfront'),
(3, 'Annie January', 'Starlight')

SET IDENTITY_INSERT Powers OFF