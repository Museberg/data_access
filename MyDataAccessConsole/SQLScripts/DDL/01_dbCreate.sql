USE master
GO
IF NOT EXISTS (
 SELECT name
 FROM sys.databases
 WHERE name = N'assignment_5'
)
 CREATE DATABASE [assignment_5];
GO