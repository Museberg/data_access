CREATE TABLE PowerSuperheroRelation (
    PowerID INT NOT NULL,
    SuperheroID INT NOT NULL,
    FOREIGN KEY (PowerId) REFERENCES Powers(ID)
        ON DELETE CASCADE,
    FOREIGN KEY (SuperheroID) REFERENCES Superheroes(ID)
        ON DELETE CASCADE,
    UNIQUE(PowerID, SuperheroID)
)