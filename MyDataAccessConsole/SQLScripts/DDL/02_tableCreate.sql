-- Use my database
USE [assignment_5];

-- 
IF OBJECT_ID('dbo.Superheroes', 'U') IS NOT NULL
DROP TABLE [Superheroes];

CREATE TABLE Superheroes(
    ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    FullName nvarchar(50) NULL,
    Alias nvarchar(50) NULL
);


IF OBJECT_ID('dbo.Assistants', 'U') IS NOT NULL
DROP TABLE [Assistants];

CREATE TABLE Assistants (
    ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    FullName nvarchar(50) NULL
);


IF OBJECT_ID('dbo.Powers', 'U') IS NOT NULL
DROP TABLE [Powers];

CREATE TABLE Powers (
    ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    FullName nvarchar(50) NULL,
    PowerDescription NVARCHAR(200) NULL
)



