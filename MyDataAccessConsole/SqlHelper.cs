using Microsoft.Data.SqlClient;

namespace MyDataAccessConsole;

public static class SqlHelper
{
    private static string ConnectionString = null;
    
    /// <summary>
    /// Singleton getter for our connection string
    /// </summary>
    /// <returns>The connection string for the database</returns>
    public static string GetConnectionString()
    {
        if (string.IsNullOrEmpty(ConnectionString))
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder()
            {
                DataSource = "localhost",
                UserID = "sa",
                Password = "#Gullwing1",
                InitialCatalog = "Chinook",
                TrustServerCertificate = true
            };
            ConnectionString = connectionStringBuilder.ConnectionString;
        }
        
        return ConnectionString;
    }
    
    /// <summary>
    /// This attempts to read a string from the database.
    /// </summary>
    /// <param name="reader">The SqlDataReader to use</param>
    /// <param name="colIndex">The column index to read from</param>
    /// <returns>Returns the read value or an empty string if null was found</returns>
    public static string SafeGetString(this SqlDataReader reader, int colIndex)
    {
        if(!reader.IsDBNull(colIndex))
            return reader.GetString(colIndex);
        return string.Empty;
    }
}