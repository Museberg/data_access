using Microsoft.Data.SqlClient;

namespace MyDataAccessConsole.Repositories.Customer;
using Models;

public class CustomerRepository : ICustomerRepository
{
    /// <summary>
    /// Fetches all customers from the database
    /// </summary>
    /// <returns>A list of all customers</returns>
    /// <exception cref="SqlException"></exception>
    public List<Customer> GetAllCustomers()
    {
        List<Customer> customers = new List<Customer>();
        string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

        try
        {
            using var conn = new SqlConnection(SqlHelper.GetConnectionString());
            conn.Open();
            
            using var cmd = new SqlCommand(sql, conn);
            using SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var temp = new Customer();
                temp.CustomerId = reader.GetInt32(0);
                temp.FirstName = reader.SafeGetString(1);
                temp.LastName = reader.SafeGetString(2);
                temp.Country = reader.SafeGetString(3);
                temp.PostalCode = reader.SafeGetString(4);
                temp.Phone = reader.SafeGetString(5);
                temp.Email = reader.SafeGetString(6);
                
                customers.Add(temp);
            }
        }
        catch (SqlException e)
        {
            Console.WriteLine(e.Message);
            throw;
        }

        return customers;
    }
    
    /// <summary>
    /// Fetches the customer with the matching Id
    /// </summary>
    /// <param name="id">The Id of the customer you wish to fetch</param>
    /// <returns>The found customer</returns>
    /// /// <exception cref="SqlException"></exception>
    public Customer GetCustomer(int id)
    {
        var customer = new Customer();
        var sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";

        try
        {
            using var conn = new SqlConnection(SqlHelper.GetConnectionString());
            conn.Open();

            using var cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@CustomerId", id);

            using var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                customer.CustomerId = reader.GetInt32(0);
                customer.FirstName = reader.SafeGetString(1);
                customer.LastName = reader.SafeGetString(2);
                customer.Country = reader.SafeGetString(3);
                customer.PostalCode = reader.SafeGetString(4);
                customer.Phone = reader.SafeGetString(5);
                customer.Email = reader.SafeGetString(6);
            }
        }
        catch (SqlException e)
        {
            Console.WriteLine(e.Message);
            throw;
        }

        return customer;
    }
    
    /// <summary>
    /// Fetches the first customer with either a matching first name or last name
    /// </summary>
    /// <param name="name">The name to search for</param>
    /// <returns>The found customer</returns>
    /// <exception cref="SqlException"></exception>
    public Customer GetCustomer(string name)
    {
        var customer = new Customer();
        string sql = 
            "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE "
             + "FirstName LIKE @Name OR LastName LIKE @Name";

        try
        {
            using var conn = new SqlConnection(SqlHelper.GetConnectionString());
            conn.Open();
            
            using var cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Name", name);

            using var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                customer.CustomerId = reader.GetInt32(0);
                customer.FirstName = reader.SafeGetString(1);
                customer.LastName = reader.SafeGetString(2);
                customer.Country = reader.SafeGetString(3);
                customer.PostalCode = reader.SafeGetString(4);
                customer.Phone = reader.SafeGetString(5);
                customer.Email = reader.SafeGetString(6);
            }
        }
        catch (SqlException e)
        {
            Console.WriteLine(e);
            throw;
        }

        return customer;
    }

    /// <summary>
    /// Returns a range of customers based on offset and limit
    /// </summary>
    /// <param name="offset">Offset to look at</param>
    /// <param name="limit">The limit of customers to return</param>
    /// <returns>A list of customers</returns>
    public List<Customer> GetRangeOfCustomers(int offset, int limit)
    {
        var customers = new List<Customer>();
        string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer "
            + "ORDER BY CustomerId OFFSET (@Offset) ROWS FETCH NEXT (@Limit) ROWS ONLY";

        try
        {
            using var conn = new SqlConnection(SqlHelper.GetConnectionString());
            conn.Open();

            using var cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Limit", limit);
            cmd.Parameters.AddWithValue("@Offset", offset);

            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var temp = new Customer();
                temp.CustomerId = reader.GetInt32(0);
                temp.FirstName = reader.SafeGetString(1);
                temp.LastName = reader.SafeGetString(2);
                temp.Country = reader.SafeGetString(3);
                temp.PostalCode = reader.SafeGetString(4);
                temp.Phone = reader.SafeGetString(5);
                temp.Email = reader.SafeGetString(6);
                
                customers.Add(temp);
            }
            
        }
        catch (SqlException e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
        
        return customers;
    }

    /// <summary>
    /// Adds a new customer to the database
    /// </summary>
    /// <param name="customer">The customer to add to the database</param>
    /// <returns>True if customer was added, false if not</returns>
    /// <exception cref="SqlException"></exception>
    public bool AddNewCustomer(Customer customer)
    {
        var success = false;
        var sql =
            "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) VALUES " +
            "(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

        try
        {
            using var conn = new SqlConnection(SqlHelper.GetConnectionString());
            conn.Open();

            using var cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
            cmd.Parameters.AddWithValue("@LastName", customer.LastName);
            cmd.Parameters.AddWithValue("@Country", customer.Country);
            cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
            cmd.Parameters.AddWithValue("@Phone", customer.Phone);
            cmd.Parameters.AddWithValue("@Email", customer.Email);

            success = cmd.ExecuteNonQuery() > 0;
        }
        catch (SqlException e)
        {
            Console.WriteLine(e.Message);
            throw;
        }

        return success;
    }

    /// <summary>
    /// Updates the information of the provided customer in the database
    /// </summary>
    /// <param name="customer"></param>
    /// <returns>True if update succeeded, false if not</returns>
    /// <exception cref="SqlException"></exception>
    public bool UpdateCustomer(Customer customer)
    {
        var success = false;
        var sql =
            "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, " +
            "Phone = @Phone, Email = @Email WHERE CustomerId = (@CustomerId)";

        try
        {
            using var conn = new SqlConnection(SqlHelper.GetConnectionString());
            conn.Open();

            using var cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
            cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
            cmd.Parameters.AddWithValue("@LastName", customer.LastName);
            cmd.Parameters.AddWithValue("@Country", customer.Country);
            cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
            cmd.Parameters.AddWithValue("@Phone", customer.Phone);
            cmd.Parameters.AddWithValue("@Email", customer.Email);

            success = cmd.ExecuteNonQuery() > 0;

        }
        catch (SqlException e)
        {
            Console.WriteLine(e);
            throw;
        }

        return success;
    }

    public bool DeleteCustomer(string id)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// Looks at how many customers comes from each country
    /// </summary>
    /// <returns>A list of how many customers has registered from each country</returns>
    /// <exception cref="SqlException"></exception>
    public List<CustomerCountry> GetCustomerCountries()
    {
        var customerCountries = new List<CustomerCountry>();
        var sql = "SELECT Country, COUNT(CustomerID) FROM Customer GROUP BY Country ORDER BY COUNT(CustomerID) DESC";

        try
        {
            using var conn = new SqlConnection(SqlHelper.GetConnectionString());
            conn.Open();

            using var cmd = new SqlCommand(sql, conn);
            using var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var temp = new CustomerCountry();
                temp.Country = reader.GetString(0);
                temp.CustomersInCountry = reader.GetInt32(1);
                customerCountries.Add(temp);
            }
        }
        catch (SqlException e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
        
        return customerCountries;
    }

    /// <summary>
    /// Returns a list of how much money each customer has spent at the store
    /// </summary>
    /// <returns>A list of each customers spending</returns>
    /// <exception cref="SqlException"></exception>
    public List<CustomerSpender> GetBiggestSpenders()
    {
        var biggestSpenders = new List<CustomerSpender>();
        var sql =
            "SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, SUM(Invoice.Total) as 'TotalSum' FROM Customer " +
            "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId GROUP BY Customer.FirstName, Customer.LastName, Customer.CustomerId ORDER BY SUM(Invoice.Total) DESC";

        try
        {
            using var conn = new SqlConnection(SqlHelper.GetConnectionString());
            conn.Open();

            using var cmd = new SqlCommand(sql, conn);
            using var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var temp = new CustomerSpender();
                temp.Customer = new Customer()
                {
                    CustomerId = reader.GetInt32(0),
                    FirstName = reader.GetString(1),
                    LastName = reader.GetString(2)
                };
                temp.MoneySpent = Decimal.ToDouble(reader.GetDecimal(3));
                biggestSpenders.Add(temp);
            }
        }
        catch (SqlException e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
        
        return biggestSpenders;
    }

    /// <summary>
    /// Finds the customer's favorite genre by looking at their purchase history
    /// </summary>
    /// <param name="id">The id of customer to look at</param>
    /// <returns>An object containing their favorite genre(s) and how many times they purchased music of that genre</returns>
    /// <exception cref="SqlException"></exception>
    public CustomerGenre GetFavoriteGenre(int id)
    {
        var favoriteGenre = new CustomerGenre();
        var sql = "SELECT Genre.Name, COUNT(Genre.Name) as 'Genre Count' FROM Customer " +
        "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
        "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
        "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
        "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
        "WHERE Customer.CustomerId = (@CustomerId) " +
        "GROUP BY Genre.Name ORDER BY 'Genre Count' DESC";

        try
        {
            using var conn = new SqlConnection(SqlHelper.GetConnectionString());
            conn.Open();

            using var cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@CustomerId", id);

            using var reader = cmd.ExecuteReader();
            var temp = new CustomerGenre();
            while (reader.Read())
            {
                var genreName = reader.GetString(0);
                var genreCount = reader.GetInt32(1);
                
                if (temp.GenreCount > genreCount)
                    continue;

                if (temp.GenreCount < genreCount)
                {
                    temp.GenreCount = genreCount;
                    temp.Genre = new List<string>() { genreName };
                }
                else if (temp.GenreCount == genreCount)
                    temp.Genre.Add(genreName);
            }

            favoriteGenre = temp;
        }
        catch (SqlException e)
        {
            Console.WriteLine(e.Message);
            throw;
        }

        return favoriteGenre;
    }
}