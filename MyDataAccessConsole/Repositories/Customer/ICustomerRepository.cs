namespace MyDataAccessConsole.Repositories.Customer;
using Models;

public interface ICustomerRepository
{
    public Customer GetCustomer(int id);
    public Customer GetCustomer(string name);
    public List<Customer> GetAllCustomers();
    public List<Customer> GetRangeOfCustomers(int offset, int limit);
    public bool AddNewCustomer(Customer customer);
    public bool UpdateCustomer(Customer customer);
    public bool DeleteCustomer(string id);

    public List<CustomerCountry> GetCustomerCountries();
    public List<CustomerSpender> GetBiggestSpenders();
    public CustomerGenre GetFavoriteGenre(int id);
}