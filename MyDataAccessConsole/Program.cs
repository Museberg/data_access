﻿// See https://aka.ms/new-console-template for more information

using Bogus;
using MyDataAccessConsole.Models;
using MyDataAccessConsole.Repositories.Customer;

ICustomerRepository repository = new CustomerRepository();
var faker = new Faker("en_US");

// 1 - Read all customers from database
Console.WriteLine("--------------- TASK 1 ---------------");
foreach (var customer in repository.GetAllCustomers())
    Console.WriteLine(customer);

// 2 - Read a customer by id
Console.WriteLine("--------------- TASK 2 ---------------");
Console.WriteLine(repository.GetCustomer(40));

// 3 - Read a customer by name (first/last- name)
Console.WriteLine("--------------- TASK 3 ---------------");
Console.WriteLine(repository.GetCustomer("Helena"));

// 4 - Read a subset of all customers - defined by offset and limit
Console.WriteLine("--------------- TASK 4 ---------------");
foreach (var customer in repository.GetRangeOfCustomers(2, 5))
    Console.WriteLine(customer);

// 5 - Add a new customer to the database
Console.WriteLine("--------------- TASK 5 ---------------");
var newCustomer = new Customer()
{
    FirstName = faker.Person.FirstName,
    LastName = faker.Person.LastName,
    Country = faker.Address.Country(),
    Email = faker.Person.Email,
    Phone = faker.Person.Phone,
    PostalCode = faker.Person.Address.ZipCode
};
repository.AddNewCustomer(newCustomer);
// Fetching customer and writing to console
newCustomer = repository.GetCustomer(newCustomer.FirstName);
Console.WriteLine(newCustomer);

// 6 - Updating an existing customer
Console.WriteLine("--------------- TASK 6 ---------------");
faker = new("en_US");
newCustomer.FirstName = faker.Person.FirstName;
newCustomer.LastName = faker.Person.LastName;
repository.UpdateCustomer(newCustomer);
// Fetching customer and writing to console
Console.WriteLine(repository.GetCustomer(newCustomer.FirstName));

// 7 - Getting amount of customers per country
Console.WriteLine("--------------- TASK 7 ---------------");
foreach (var customerCountry in repository.GetCustomerCountries())
    Console.WriteLine(customerCountry);

// 8 - Highest spending customers
Console.WriteLine("--------------- TASK 8 ---------------");
foreach (var biggestSpender in repository.GetBiggestSpenders())
    Console.WriteLine(biggestSpender);

// 9 - Most popular genre for a given customer
Console.WriteLine("--------------- TASK 9 ---------------");
Console.WriteLine(repository.GetFavoriteGenre(12));