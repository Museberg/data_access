namespace MyDataAccessConsole.Models;
using System.Linq;

public class CustomerGenre
{
    public List<string> Genre { get; set; }
    public int GenreCount { get; set; }
    
    public override string ToString()
    {
        if (Genre.Count > 1)
            return $"{string.Join(", ", Genre)}: {GenreCount}";

        return $"{Genre[0]}: {GenreCount}";
    }
}