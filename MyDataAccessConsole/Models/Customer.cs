using System.Text;

namespace MyDataAccessConsole.Models;

public class Customer
{
    public int CustomerId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Country { get; set; }
    public string PostalCode { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }


    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.AppendLine($"Customer id: {CustomerId}");
        sb.AppendLine($"First name: {FirstName}");
        sb.AppendLine($"Last name: {LastName}");
        sb.AppendLine($"Country: {Country}");
        sb.AppendLine($"Postal code: {PostalCode}");
        sb.AppendLine($"Phone: {Phone}");
        sb.AppendLine($"Email: {Email}");
        
        return sb.ToString();
    }
}