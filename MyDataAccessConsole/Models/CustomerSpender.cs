namespace MyDataAccessConsole.Models;

public class CustomerSpender
{
    public Customer Customer { get; set; }
    public double MoneySpent { get; set; }

    public override string ToString()
    {
        return $"{Customer.FirstName} {Customer.LastName}: {MoneySpent}";
    }
}