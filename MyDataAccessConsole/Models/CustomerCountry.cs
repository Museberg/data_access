using System.Text;

namespace MyDataAccessConsole.Models;

public class CustomerCountry
{
    public string Country { get; set; }
    public int CustomersInCountry { get; set; }

    public override string ToString()
    {
        return $"{Country}: {CustomersInCountry}";
    }
}